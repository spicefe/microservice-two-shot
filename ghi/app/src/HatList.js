function HatsList(props) {
    function refreshHats() {
        window.location.reload(false);
    }

    async function deleteHat(id) {
        const hatURL = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
            refreshHats();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Style Name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                return (
                    <tr key={hat.id}>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.color }</td>
                        <td><img src={ hat.picture_url } height="150" width="150"/></td>
                        <td>{ hat.location }</td>
                        <td><button className="btn btn-danger" onClick={() => {deleteHat(hat.id)}}>Delete</button></td>
                    </tr>
                );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;

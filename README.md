# Wardrobify

Team:

* Eli - Hats
* Person 2 - Hugo Rodriguez(Shoes)

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

For my models, I created a Hats model and a LocationVO model.

The Hats model contains all of the relevant information regarding the hats: fabric, style_name, color, picture_url, and location as a foreign key to LocationVO.

The LocationVO model contains the href as well as the closet name and section and shelf numbers.

Afterwards, I created my model encoders for the LocationVO and Hats, as well as making the View functions for hats.

Next I linked everything together with the URLs and started working on polling the location data so I could start making proper model data.

I polled the location data from the wardrobe microservice into my hats microservice and put the information inside a LocationVO object.

After, I began integrating my backend with react and made the Hat List page, making it appear with a list of all of the hats, with their details and where they are located. I integrated the page into the NavBar as well, utilizing React Router. Next, I did the same but with the Hat Form. I created the form and stored the data in the database properly, integrating the link to the page in the NavBar after.

Lastly, I integrated the delete function on the Hat List page, having to grab the hat id from the proper area and using the delete method I created earlier in the views. Once the function ran and it was successful, I integrated a refresh function that would automatically refresh the page. On the button I added an on-click that would activate the function when the delete button was pressed. I made the button look pretty as well.

I wish I could have worked on this longer and made it more delightful, but I hope you enjoyed what I did with the time I had! Thanks for reading and have a wonderful rest of your day!
